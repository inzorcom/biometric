(function($) { 	    

/**
 * Provide and configure jquery datepicker
 */  
Drupal.behaviors.clt_biometric_datepicker = {
  attach: function (context) {  
    var drawDatePickerBox = function (date_string) {                
      var date_object = new Date(date_string);
      var month=new Array();
      month[0]="Jan";
      month[1]="Feb";
      month[2]="March";
      month[3]="April";
      month[4]="May";
      month[5]="June";
      month[6]="Jul";
      month[7]="Aug";
      month[8]="Sep"; 
      month[9]="Oct"; 
      month[10]="Nov"; 
      month[11]="Dec";          
      $('.dateBoxInner .month').text(month[date_object.getUTCMonth()]);
      $('.dateBoxInner .day').text(date_object.getUTCDate());                          
    };

    $('#symptomDateBox').bind('click', function () {
      $("#edit-date").trigger('focus');                                        
    });        
    $("#edit-date").datepicker({maxDate: "0D", dateFormat: "yy-mm-dd",
      onSelect: function(dateText, inst) {
        drawDatePickerBox(dateText);
//        window.location = '/biometric/' + dateText;           
      }
    });              

    drawDatePickerBox($("#edit-date").val());     
  }
};    
  
})(jQuery);
