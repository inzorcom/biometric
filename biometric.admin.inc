<?php
/**
 * @file
 * Admin side functionality of the module
 * 
 */

/**
 * @file
 * Biometric editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring instead to use
 * views. That offers more flexibility to change a UI that will, more often than not,
 * be end-user facing.
 */

/**
 * UI controller.
 */
class BiometricUIController extends EntityDefaultUIController {
  
  public function hook_menu() {
   $items = array();
   $id_count = count(explode('/', $this->path));
   $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

  $items[$this->path] = array(
    'title' => 'Biometrics',
    'description' => 'Add edit and update biometrics.',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('access administration pages'),
    'file path' => drupal_get_path('module', 'system'),
    'file' => 'system.admin.inc',
  );
    
  $items[$this->path]['type'] = MENU_LOCAL_TASK;
  
  $items[$this->path. '/add'] = array(
    'title'            => 'Add biometric data',
    'description' => 'Add a new biometric data',
    'page callback'  => 'biometric_add_page',
    'access callback'  => 'biometric_access',
    'access arguments' => array('edit'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'biometric.admin.inc',
    'file path' => drupal_get_path('module', $this->entityInfo['module'])
  );
  
  foreach (biometric_get_types() as $type) {
    $items[$this->path . '/add/' . $type->type] = array(
      'title' => 'Add ' . $type->label,
      'page callback' => 'biometric_form_wrapper',
      'page arguments'   => array(biometric_create(array('type' => $type->type))),
      'access callback' => 'biometric_access',
      'access arguments' => array('edit', 'edit ' . $type->type),
      'file' => 'biometric.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );
  }
  
  $items[$this->path . '/biometric/' . $wildcard] = array(
    'page callback' => 'biometric_form_wrapper',
    'page arguments' => array($id_count + 1),
    'access callback' => 'biometric_access',
    'access arguments' => array('edit', $id_count + 1),
    'weight' => 0,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    'file' => 'biometric.admin.inc',
    'file path' => drupal_get_path('module', $this->entityInfo['module'])
  );

  $items[$this->path.'/'. $wildcard. '/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'biometric_delete_form_wrapper',
    'page arguments' => array($id_count + 1),
    'access callback' => 'biometric_access',
    'access arguments' => array('edit', $id_count + 1),
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'file' => 'biometric.admin.inc',
    'file path' => drupal_get_path('module', $this->entityInfo['module'])
  );

  $items[$this->path.'/'. $wildcard. '/edit'] = array(
    'title' => 'Edit',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
  );
  
  $items['biometric/'.$wildcard] = array(
    'title callback' => 'biometric_page_title',
    'title arguments' => array(1),
    'page callback' => 'biometric_page_view',
    'page arguments' => array(1),
    'access callback' => 'biometric_access',
    'access arguments' => array('view', 1),
    'type' => MENU_CALLBACK,
  );

  return $items;
  }
  
    /**
   * Create the markup for the add Biometric Entities page within the class
   * so it can easily be extended/overriden.
   */ 
  public function addPage() {
    $item = menu_get_item();
    $content = system_admin_menu_block($item);

    if (count($content) == 1) {
      $item = array_shift($content);
      drupal_goto($item['href']);
    }    
        
    return theme('biomeric_add_list', array('content' => $content));
  }
}

/**
 * Form callback wrapper: create or edit a biometric.
 *
 * @param $biometric
 *   The biometric object being edited by this form.
 *
 * @see biometric_edit_form()
 */
function biometric_form_wrapper($biometric) {
  return drupal_get_form('biometric_edit_form', $biometric);
}

/**
 * Form callback: create or edit a biometric.
 *
 * @param $biometric
 *   The biometric object to edit or for a create form an empty biometric object
 *   
 */
function biometric_edit_form($form, &$form_state, $biometric) {

$form['date'] = array(
  '#title' => t('Date'),
  '#type' => 'textfield',
  '#default_value' => isset($biometric->date)? $biometric->date : date('Y-m-d', time()),
  //'#suffix' => '<span class="dateBoxInner"><span class="month">'.$month.'</span><span class="day"> '.$day.'</span></span>',
);

$form['notes'] = array (
  '#prefix'        => '<div class="commentBox">
                       <span class="label"></span>
                       <span class="field">',
  '#title'         => t('Notes'),
  '#type'          => 'textarea',
  '#default_value' => isset($biometric->notes)? $biometric->notes : '',
  '#resizable'     => TRUE,
  '#suffix'        => '</span></div>',
);

// Add the field related form elements.
  $form_state['biometric'] = $biometric;
  field_attach_form('biometric', $biometric, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );
  $path = drupal_get_path('module', 'biometric');
  $form['#attached']['css'] = array($path.'/biometric.css');
  $form['#attached']['js'] = array($path.'/biometric.js');

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save data'),
    '#submit' => $submit + array('biometric_edit_form_submit'),
  );
  
  if (!empty($biometric->date)) {
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete biometric'),
    '#suffix' => l(t('Cancel'), 'admin/content/biometrics'),
    '#submit' => $submit + array('biometric_form_submit_delete'),
    '#weight' => 45,
  );
  }

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'biometric_edit_form_validate';
  return $form;
}

/**
 * Form API validate callback for the biometric form
 */
function biometric_edit_form_validate(&$form, &$form_state) {
  $biometric = $form_state['biometric'];
  
  // Notify field widgets to validate their data.
  field_attach_form_validate('biometric', $biometric, $form, $form_state);
}


/**
 * Form API submit callback for the biometric form.
 * 
 * @todo remove hard-coded link
 */
function biometric_edit_form_submit(&$form, &$form_state) {
  global $user;
  //echo var_dump($form);  die;
  $biometric = entity_ui_controller('biometric')->entityFormSubmitBuildEntity($form, $form_state);
  // Save the biometric and go back to the list of biometric
  $biometric->uid = $user->uid;
  if ($biometric->is_new = isset($biometric->is_new) ? $biometric->is_new : 0){
    $biometric->created = time();
  }

  $biometric->changed = time();
  $biometric->save();
  $form_state['redirect'] = 'admin/content/biometrics';
}

/**
 * Form API submit callback for the delete button.
 * 
 * @todo Remove hard-coded path
 */
function biometric_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/content/biomerics/biometric/' . $form_state['biometric']->bid . '/delete';
}


/**
 * Form callback: confirmation form for deleting a biometric.
 *
 * @param $biometric
 *   The biometric to delete
 *
 * @see confirm_form()
 */
function biometric_delete_form($form, &$form_state, $biometric) {
  $form_state['biometric'] = $biometric;

  $form['#submit'][] = 'biometric_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete biometric?'),
    'admin/content/biometrics/biometric',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );
  
  return $form;
}

/**
 * Submit callback for biometric_delete_form
 */
function biometric_delete_form_submit($form, &$form_state) {
  $biometric = $form_state['biometric'];

  biometric_delete($biometric);

  drupal_set_message(t('The biometric %id has been deleted.', array('%id' => $biometric->bid)));
  watchdog('biometric', 'Deleted biometric %id.', array('%id' => $biometric->bid));

  $form_state['redirect'] = 'admin/content/biometrics';
}

/**
 * Page to add Biometric Entities.
 * 
 */
function biometric_add_page() {
  $controller = entity_ui_controller('biometric');
  return $controller->addPage();
}

/**
 * Displays the list of available biometric types for biometric creation.
 *
 * @ingroup themeable
 */
function theme_biometric_add_list($variables) {
  $content = $variables['content'];
  $output = '';
  if ($content) {
    $output = '<dl class="biometric-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href']) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    if (user_access('administer biometric types')) {
      $output = '<p>' . t('Biometric Entities cannot be added because you have not created any biometric types yet. Go to the <a href="@create-biometric-type">biometric type creation page</a> to add a new biometric type.', array('@create-biometric-type' => url('admin/structure/biometric_types/add'))) . '</p>';
    }
    else {
      $output = '<p>' . t('No biometric types have been created yet for you to use.') . '</p>';
    }
  }

  return $output;
}