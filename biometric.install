<?php
/**
 * @file
 * Contains install functions for biometric module
 *
 */

/**
 * Implements hook_schema().
 *
 */
function biometric_schema() {   
  
  $schema['biometric'] = array( 
    'description' => 'The table for biometric data.', 
    'fields' => array(
      'bid' => array(
        'description' => 'The primary identifier for a biometric data.',
        'type'        => 'serial',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
      ),
      'uid' => array(
        'description' => 'The {users}.uid that saved symptom.',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
      ),
      'type' => array(
        'description' => 'The {biometric_type}.type of this biometric.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the biometric was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the biometric was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'language' => array(
        'description' => 'The language of the biometric.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'date' => array(
        'description' => 'The date when data  of symptoms was saved.',
        'type'        => 'char',
        'length'      => 10,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'notes' => array(
        'description' => 'Notes of the day when the instance was created',
        'type'        => 'text',
        'size'        => 'normal',
        'not null'    => FALSE,
      )
    ),     
    'primary key' => array('bid'),
    'indexes' => array(
       'b_dates' => array('date'),
       'type' => array('type'),
    ),
  );
  
  $schema['biometric_type'] = array(
    'description' => 'Stores information about defined biometric types.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique biometric type identifier.',
      ),
      'type' => array(
        'description' => 'The machine-readable name of this biometric type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'The human-readable name of this biometric type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'A brief description of this type.',
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'medium',
        'translatable' => TRUE,
      ),
      'weight' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
        'description' => 'The weight of this biometric type in relation to others.',
      ),
    ) + entity_exportable_schema_fields(),
    'primary key' => array('id'),
    'unique keys' => array(
      'type' => array('type'),
    ),
  );
  
  return $schema;
}