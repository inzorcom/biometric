<?php

/**
 * @file
 * Biometric type editing UI.
 */

/**
 * UI controller.
 */
class BiometricTypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
		$items[$this->path]['description'] = 'Manage biometric entity types, including adding
		and removing fields and the display of fields.';
    return $items;
  }
}

/**
 * Generates the biometric type editing form.
 */
function biometric_type_form($form, &$form_state, $biometric_type, $op = 'edit') {

  if ($op == 'clone') {
    $biometric_type->label .= ' (cloned)';
    $biometric_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $biometric_type->label,
    '#description' => t('The human-readable name of this biometric type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  
  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#default_value' => $biometric_type->description,
    '#description' => t('Describe this content type. The text will be displayed on the <em>Add new content</em> page.'),
  );
 
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($biometric_type->type) ? $biometric_type->type : '',
    '#maxlength' => 32,

    '#machine_name' => array(
      'exists' => 'biometric_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this biometric type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save biometric type'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function biometric_type_form_submit(&$form, &$form_state) {
  $biometric_type = entity_ui_form_submit_build_entity($form, $form_state);
  $biometric_type->save();
  $form_state['redirect'] = 'admin/structure/biometric_types';
}

/**
 * Form API submit callback for the delete button.
 */
function biometric_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/biometric_types/manage/' . $form_state['biometric_type']->type . '/delete';
}
